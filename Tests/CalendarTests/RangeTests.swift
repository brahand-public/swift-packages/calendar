import XCTest
@testable import Calendar

final class CalendarRangeTests: XCTestCase {

    var calendars: [CalendarTimeRange: CalendarDateModel] = [:]

    override func setUp() {
        calendars = [
           .daily: CalendarDateModel(timeUnit: .daily),
           .weekly: CalendarDateModel(timeUnit: .weekly),
           .monthly: CalendarDateModel(timeUnit: .monthly),
           .yearly: CalendarDateModel(timeUnit: .monthly)
       ]
    }

    func testPerformanceExample() throws {
        self.measure {
            let _ = CalendarDateModel(timeUnit: .weekly)
        }
    }

    func testInitialDateRanges() throws {
        XCTAssertEqual(calendars[.daily]?.datesRange.count, 1) // 1 day
        XCTAssertEqual(calendars[.weekly]?.datesRange.count, 7) // 7 days
        XCTAssertEqual(calendars[.monthly]?.datesRange.count, 5) // 5 months
        XCTAssertEqual(calendars[.yearly]?.datesRange.count, 5) // 5 years
    }

    func testForwardIterableDateRanges() throws {
        calendars[.daily]?.moveRangeForward()
        calendars[.weekly]?.moveRangeForward()
        calendars[.monthly]?.moveRangeForward()
        calendars[.yearly]?.moveRangeForward()

        XCTAssertEqual(calendars[.daily]?.datesRange.count, 1) // 1 day
        XCTAssertEqual(calendars[.weekly]?.datesRange.count, 7) // 7 days
        XCTAssertEqual(calendars[.monthly]?.datesRange.count, 5) // 5 months
        XCTAssertEqual(calendars[.yearly]?.datesRange.count, 5) // 5 years
    }

    func testBackwardIterableDateRanges() throws {
        calendars[.daily]?.moveRangeBackward()
        calendars[.weekly]?.moveRangeBackward()
        calendars[.monthly]?.moveRangeBackward()
        calendars[.yearly]?.moveRangeBackward()

        XCTAssertEqual(calendars[.daily]?.datesRange.count, 1) // 1 day
        XCTAssertEqual(calendars[.weekly]?.datesRange.count, 7) // 7 days
        XCTAssertEqual(calendars[.monthly]?.datesRange.count, 5) // 5 months
        XCTAssertEqual(calendars[.yearly]?.datesRange.count, 5) // 5 years
    }
}
