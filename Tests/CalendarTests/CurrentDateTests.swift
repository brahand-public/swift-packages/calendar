//
//  CalendarCurrentDateTests.swift
//  
//
//  Created by Brahand on 4/10/22.
//

import XCTest
@testable import Calendar

class CalendarCurrentDateTests: XCTestCase {

    let weeklyCalendar = CalendarDateModel(timeUnit: .weekly)

    func testAucklandCurrentDate() throws {
        let currentDateFormat = CalendarDateModel(
            timeUnit: .daily,
            calendar: Calendar.autoupdatingCurrent,
            timeZone: TimeZone(identifier: "Pacific/Auckland")!
        ).datesRangeTitle()

        let aucklandDate = Date()
        XCTAssertEqual(currentDateFormat, aucklandDate.formatted(.dateTime.month(.wide).day()), "\(Locale.current)")
    }
}
