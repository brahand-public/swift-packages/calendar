//
//  TimeZoneTests.swift
//  
//
//  Created by Brahand on 4/10/22.
//

import XCTest
@testable import Calendar

class TimeZoneTests: XCTestCase {
    // Auckland, New Zealand is 12 hours ahead of Coordinated Universal Time
    let AucklandTimeZone = TimeZone(identifier: "Pacific/Auckland")!

    func testCompareAucklandTimeZone() throws {
        let currentDate = CalendarDateModel(timeUnit: .daily, timeZone: AucklandTimeZone, locale: Locale(identifier: "en")).currentDate

        // Test scheme time zone (TZ) is set to Auckland
        let aucklandDate = Date()

        XCTAssertEqual(currentDate.formatted(), aucklandDate.formatted())
    }
}
