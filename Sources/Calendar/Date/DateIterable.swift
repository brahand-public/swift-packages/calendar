//
//  DateIterable.swift
//  
//
//  Created by Brahand on 7/31/22.
//

import Foundation

extension CalendarDateModel: CalendarIterable {

    /// Method to mutate the selected date to the current date and calculate again the
    /// dates based on the current time range ( weekly, monthly, ... )
    ///
    /// First, it calculates the starting date of the current range, from there we count the
    /// number of items in that range and finally we sum those units with the first date.
    ///
    public func moveToCurrentDate() {
        var datesRange: [Date] = []
        let rangeStart = rangeStartAroundDate(self.currentDate)
        for step in 0..<self.timeRange.unitsCount {
            let stepRange = convertInteger(step, to: timeRange)
            datesRange.append(calendar.date(byAdding: stepRange, to: rangeStart)!)
        }

#if DEBUG
        for (index, date) in datesRange.enumerated() {
            print("Current range #\(index): \(date.formatted())")
        }
#endif
        self.datesRange = datesRange
    }

    /// Method to calculate the next range of dates
    ///
    /// We start with the last date of the current range and then we append
    /// the following dates
    ///
    public func moveRangeForward(){
        var nextRange: [Date?] = []
        /// Note: we are starting the next range with the last date of the current one
        /// and we are adding 1 unit to make it the next date.
        let unitStep = self.convertInteger(1, to: self.timeRange)
        let startDate = calendar.date(byAdding: unitStep, to: self.lastRangeDate)!
        for step in 0..<self.timeRange.unitsCount {
            let stepRange = self.convertInteger(step, to: self.timeRange)
            nextRange.append(calendar.date(byAdding: stepRange, to: startDate))
        }

#if DEBUG
        for (index, date) in nextRange.compactMap({ $0 }).enumerated() {
            print("Next range #\(index): \(date.formatted())")
        }
#endif

        self.datesRange = nextRange.compactMap{ $0 }
    }

    /// Method to calculate the previous range of dates
    ///
    /// We start with the first date of the current range and then we append
    /// the previous dates
    public func moveRangeBackward() {
        var previousRange: [Date?] = []
        /// Note: we are starting the previous range with the range difference with firstRangeDate
        /// and we are removing 1 range (units count) to calculate the previous date.
        let unitStep = self.convertInteger(-self.timeRange.unitsCount, to: self.timeRange)
        let startDate = calendar.date(byAdding: unitStep, to: self.firstRangeDate)!
        for step in 0..<self.timeRange.unitsCount {
            let stepRange = self.convertInteger(step, to: self.timeRange)
            previousRange.append(calendar.date(byAdding: stepRange, to: startDate))
        }

#if DEBUG
        for (index, date) in previousRange.compactMap({ $0 }).enumerated() {
            print("Previous range #\(index): \(date.formatted())")
        }
#endif

        self.datesRange = previousRange.compactMap{ $0 }
    }
}

