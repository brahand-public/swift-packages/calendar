//
//  DateOperable.swift
//  
//
//  Created by Brahand on 7/31/22.
//

import Foundation

extension CalendarDateModel: CalendarOperable {

    // Transforms an integer into a DateComponent based on the time range (daily, weekly, ...)
    // This method helps when performing similar date calculations and the only difference
    // is the time range
    public func convertInteger(_ integer: Int, to timeRange: CalendarTimeRange) -> DateComponents {
        switch timeRange {
        case .daily:    return DateComponents(calendar: calendar, timeZone: timeZone, day: integer)
        case .weekly:   return DateComponents(calendar: calendar, timeZone: timeZone, day: integer)
        case .monthly:  return DateComponents(calendar: calendar, timeZone: timeZone, month: integer)
        case .yearly:   return DateComponents(calendar: calendar, timeZone: timeZone, year: integer)
        }
    }
}

