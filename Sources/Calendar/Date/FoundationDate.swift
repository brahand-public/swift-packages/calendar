//
//  FoundationDate.swift
//  
//
//  Created by Brahand on 7/31/22.
//

import Foundation

/// Specialized class conforming protocol `CalendarDateProtocol` which defines the base
/// properties to interact with dates. This class uses SwiftDate as dependency and the
/// main type is `Date`. If other library is preferred you should create another
/// class and conform the same protocol
///
public final class CalendarDateModel: CalendarInitiable {
    public private(set) var calendar: Calendar
    public private(set) var timeZone: TimeZone
    public private(set) var locale: Locale

    public typealias CalendarDate = Date

    @Published
    public var timeRange: CalendarTimeRange {
        didSet { self.moveToCurrentDate() }
    }

    @Published
    public var datesRange: [CalendarDate] = []

    public var currentDate:  Date {
        let date = Date()
        print(date)
        print(date.formatted())
        return date
    }

    public var firstRangeDate: Date { self.datesRange.first ?? currentDate }
    public var lastRangeDate: Date { self.datesRange.last ?? currentDate }

    /// First element of the dates range i.e. first date of the range
    /// i.e. yyyy-mm-ddT00:00:00
    public var leadingDate: Date {
        guard let rangeStart = self.datesRange.first else { return Date() }
        return rangeStart
    }

    /// Last element of the dates range i.e. last date of the range
    /// i.e. yyyy-mm-ddT23:59:59
    public var trailingDate: Date {
        guard let rangeEnd = self.datesRange.last else { return Date() }
        let tomorrow = calendar.date(byAdding: .day, value: 1, to: rangeEnd)!
        let endOfDay = calendar.date(byAdding: .second, value: -1, to: tomorrow)!
        return endOfDay
    }

    lazy private var localDateFormatter: DateFormatter = {
        return DateFormatter()
    }()

    public required init(
        timeUnit : CalendarTimeRange = CalendarDateModel.DefaultTimeUnit,
        calendar : Calendar = Calendar.autoupdatingCurrent,
        timeZone : TimeZone = TimeZone.autoupdatingCurrent,
        locale   : Locale = Locale.autoupdatingCurrent
    ){
        self.timeRange  = timeUnit
        self.calendar   = calendar
        self.timeZone   = timeZone
        self.locale     = locale
        self.calendar.timeZone  = timeZone
        self.calendar.locale    = locale

        self.moveToCurrentDate()
        print(Self.DefaultTimeUnit)
        print(Self.DefaultRange.leading.formatted())
        print(Self.DefaultRange.trailing.formatted())
    }

    /// Given a date it returns the first date in the current range
    /// i.e. If the time range is weekly and date is wednesday
    /// it returnt the day for Sunday
    public func rangeStartAroundDate(_ date: Date) -> Date {
        switch self.timeRange {
        case .daily:
            return calendar.startOfDay(for: date)
        case .weekly:
            return calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: date))!
        case .monthly:
            let monthDate = calendar.date(from: calendar.dateComponents([.year, .month], from: date))!
            return calendar.date(byAdding: .month, value: -2, to: monthDate)!
        case .yearly:
            let yearDate = calendar.date(from: calendar.dateComponents([.year], from: date))!
            return calendar.date(byAdding: .year, value: -3, to: yearDate)!
        }
    }
}

extension CalendarDateModel: CalendarRepresentable {
    public func datesRangeTitle() -> String {
        var localized = Date.FormatStyle.dateTime
        localized.locale = self.locale
        localized.timeZone = self.timeZone

        switch timeRange {
        case .daily:
            return firstRangeDate.formatted(localized.day().month(.wide))
        case .weekly:
            return "\(firstRangeDate.formatted(localized.day().month(.wide))) - \(lastRangeDate.formatted(localized.day().month(.wide)))"
        case .monthly:
            return "\(firstRangeDate.formatted(localized.month(.wide).year())) - \(lastRangeDate.formatted(localized.month(.wide).year()))"
        case .yearly:
            return "\(firstRangeDate.formatted(localized.year())) - \(lastRangeDate.formatted(localized.year()))"
        }
    }
}

extension CalendarDateModel {
    public static let DefaultTimeUnit: CalendarTimeRange = .weekly

    public static var DefaultRange: (leading: Date, trailing: Date) {
        let calendar = Calendar.current
        let startOfWeek = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))!
        let nextWeek = calendar.date(byAdding: .day, value: 7, to: startOfWeek)!
        let endOfWeek = calendar.date(byAdding: .second, value: -1, to: nextWeek)!
        return (leading: startOfWeek, trailing: endOfWeek)
    }
}

public typealias CalendarRangeBarView = CalendarControlView<CalendarDateModel>

