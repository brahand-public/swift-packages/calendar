//
//  CalendarTimeUnit.swift
//  
//
//  Created by Brahand on 4/9/22.
//

import Foundation

public enum CalendarTimeRange {
    case daily, weekly, monthly, yearly

    var unitsCount: Int {
        switch self {
        case .daily   : return CalendarTimeRange.DaysCount
        case .weekly  : return CalendarTimeRange.WeeksCount
        case .monthly : return CalendarTimeRange.MonthsCount
        case .yearly  : return CalendarTimeRange.YearsCount
        }
    }
}

extension CalendarTimeRange {
    public static let DaysCount   = 1
    public static let WeeksCount  = 7
    public static let MonthsCount = 5
    public static let YearsCount  = 5
}
