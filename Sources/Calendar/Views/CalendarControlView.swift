//
//  CalendarControlView.swift
//  
//
//  Created by Brahand on 4/9/22.
//

import SwiftUI

struct CalendarControlLayout {
    static let outterWidth = 40.0
    static let innerWidth = 50.0
}

public struct CalendarItem {
    public typealias Action = () -> ()
    let image: String
    let action: Action?

    public init(image: String, action: Action?){
        self.image = image
        self.action = action
    }
}

/// Button like view to control and iterate between a range of dates. It provides
/// an interface to manipulate the current date range through a protocol exposed
/// methods
///
public struct CalendarControlView<T: CalendarRepresentable>: View {
    @EnvironmentObject var calendarModel: T

    let leftItem: CalendarItem?
    let rightItem: CalendarItem?

    public init(leftItem: CalendarItem? = nil, rightItem: CalendarItem? = nil) {
        self.leftItem = leftItem
        self.rightItem = rightItem
    }

    public var body: some View {
        RoundedRectangle(cornerRadius: 10.0)
            .fill(
                LinearGradient(
                    colors: [
                        Color.blue.opacity(0.9),
                        Color.purple.opacity(0.9)
                    ],
                    startPoint: .leading,
                    endPoint: .trailing
                )
            )
            .overlay(
                HStack(spacing: 0) {
#if !os(watchOS)
                    if let leftItem = leftItem {
                        self.item(leftItem)
                        Spacer()
                    } else {
                        EmptyView()
                    }
#endif
                    LeftArrowView
                    CalendarDateTitleView
                    RightArrowView
#if !os(watchOS)
                    if let rightItem = rightItem {
                        Spacer()
                        self.item(rightItem)
                    } else {
                        EmptyView()
                    }
#endif
                }
                    .foregroundColor(.white)
                #if !os(watchOS)
                    .buttonStyle(.borderless)
                #else
                    .buttonStyle(.plain)
                #endif
            )
    }

    private var CalendarDateTitleView: some View {
        Button {
            self.calendarModel.moveToCurrentDate()
        } label: {
            HStack {
                Text(self.calendarModel.datesRangeTitle())
                    .bold()
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .font(.subheadline)
            }
        }
        .frame(maxWidth: .infinity)
    }

    private var LeftArrowView: some View {
        Button {
            self.calendarModel.moveRangeBackward()
        } label: {
            Image(systemName: "arrow.left")
                .frame(maxWidth: .infinity, maxHeight: .infinity)
        }
        .frame(width: CalendarControlLayout.innerWidth)
    }

    private var RightArrowView: some View {
        Button {
            self.calendarModel.moveRangeForward()
        } label: {
            Image(systemName: "arrow.right")
                .frame(maxWidth: .infinity, maxHeight: .infinity)
        }
        .frame(width: CalendarControlLayout.innerWidth)
    }

    private func item(_ item: CalendarItem) -> some View {
        Button {
            item.action?()
        } label: {
            Image(systemName: item.image)
        }
        .frame(maxWidth: CalendarControlLayout.outterWidth, maxHeight: .infinity)
    }
}
