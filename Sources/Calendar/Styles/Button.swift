//
//  FlatButtonStyle.swift
//  
//
//  Created by Brahand on 4/10/22.
//

import Foundation
import SwiftUI

struct FlatButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
#if !os(watchOS)
            .buttonStyle(.borderless)
#else
            .buttonStyle(.plain)
#endif
    }
}
