//
//  CalendarRepresentable.swift
//  
//
//  Created by Brahand on 4/9/22.
//

import Foundation

public protocol CalendarRepresentable: ObservableObject, CalendarInitiable, CalendarOperable, CalendarIterable {
    func datesRangeTitle() -> String
}
