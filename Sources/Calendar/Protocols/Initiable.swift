//
//  CalendarInitiable.swift
//  
//
//  Created by Brahand on 4/9/22.
//

import Foundation

public protocol CalendarInitiable {
    associatedtype CalendarDate

    var calendar: Calendar { get }
    var timeZone: TimeZone { get }
    var locale: Locale { get }

    var timeRange: CalendarTimeRange { get set }
    var datesRange: [CalendarDate] { get set }

    var currentDate: CalendarDate { get }
    
    var firstRangeDate: CalendarDate { get }
    var lastRangeDate: CalendarDate { get }

    var leadingDate: CalendarDate { get }
    var trailingDate: CalendarDate { get }

    init(timeUnit: CalendarTimeRange, calendar: Calendar, timeZone: TimeZone, locale: Locale)

    func rangeStartAroundDate(_ date: CalendarDate) -> CalendarDate
}
