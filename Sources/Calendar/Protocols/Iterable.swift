//
//  CalendarIterable.swift
//  
//
//  Created by Brahand on 4/9/22.
//

import Foundation

public protocol CalendarIterable {
    func moveToCurrentDate()
    func moveRangeForward()
    func moveRangeBackward()
}
