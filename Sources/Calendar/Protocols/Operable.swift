//
//  CalendarOperable.swift
//  
//
//  Created by Brahand on 4/9/22.
//

import Foundation

public protocol CalendarOperable {
    func convertInteger(_ integer: Int, to timeRange: CalendarTimeRange) -> DateComponents
}
