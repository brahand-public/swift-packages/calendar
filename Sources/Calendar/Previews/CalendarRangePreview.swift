//
//  File 2.swift
//  
//
//  Created by Brahand on 8/5/22.
//

import SwiftUI

/// Previews don't use the `TZ` environment variable in the scheme
/// to define an specific timezone
struct CalendarRangeView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CalendarRangeBarView(
                leftItem: CalendarItem(image: "slider.horizontal.3", action: nil),
                rightItem: CalendarItem(image: "calendar", action: nil)
            )
            .previewLayout(.fixed(width: 400, height: 50))
            .environmentObject(CalendarDateModel(timeUnit: .daily))

            Divider().previewLayout(.fixed(width: 400.0, height: 1.0))

            ForEach(["en", "es", "fr"], id: \.self) { locale in
                CalendarRangeBarView()
                    .previewLayout(.fixed(width: 400, height: 50))
                    .environmentObject(
                        CalendarDateModel(
                            timeUnit: .daily,
                            calendar: Calendar.autoupdatingCurrent,
                            timeZone: TimeZone(identifier: "Pacific/Auckland")!,
                            locale: Locale(identifier: locale)
                        )
                    )
            }

            Divider().previewLayout(.fixed(width: 400.0, height: 1.0))

            ForEach(["en", "es", "fr"], id: \.self) { locale in
                CalendarRangeBarView()
                    .previewLayout(.fixed(width: 400, height: 50))
                    .environmentObject(
                        CalendarDateModel(
                            timeUnit: .weekly,
                            calendar: Calendar.autoupdatingCurrent,
                            timeZone: TimeZone(identifier: "Pacific/Auckland")!,
                            locale: Locale(identifier: locale)
                        )
                    )
            }

            Divider().previewLayout(.fixed(width: 400.0, height: 1.0))

            ForEach(["en", "es", "fr"], id: \.self) { locale in
                CalendarRangeBarView()
                    .previewLayout(.fixed(width: 400, height: 50))
                    .environmentObject(
                        CalendarDateModel(
                            timeUnit: .monthly,
                            calendar: Calendar.autoupdatingCurrent,
                            timeZone: TimeZone(identifier: "Pacific/Auckland")!,
                            locale: Locale(identifier: locale)
                        )
                    )
            }

            Divider().previewLayout(.fixed(width: 400.0, height: 1.0))

            ForEach(["en", "es", "fr"], id: \.self) { locale in
                CalendarRangeBarView()
                    .previewLayout(.fixed(width: 400, height: 50))
                    .environmentObject(
                        CalendarDateModel(
                            timeUnit: .yearly,
                            calendar: Calendar.autoupdatingCurrent,
                            timeZone: TimeZone(identifier: "Pacific/Auckland")!,
                            locale: Locale(identifier: locale)
                        )
                    )
            }
        }
    }
}
