# Calendar

This package is intended to provide different components that could be used by another framework. We define a generic protocol and deliver a concrete implementation which could be extended by anyone. 

## Protocols

- `CalendarDateProtocol`, is the main protocol and each component works with any concrete implementation

## Components

- `CalendarControlView`, is a horizontal button with navigation controls, current range date title and 2 buttons for configuration or custom behaviour


### TODOs

- [ ] Update readme with new protocols
- [X] Separate the main protocol in different smaller ones

## NOTES

- When swift tools version is updated, we should remove the `Package.resolved` file and build the package again `swift build -v`
